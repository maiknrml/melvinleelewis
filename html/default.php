<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="icon" href="../../../../favicon.ico" />

    <!-- Favicons -->
    <link href="img/favicon.png" rel="icon" />
    <link href="img/apple-touch-icon.png" rel="apple-touch-icon" />

    <!-- Google Fonts -->
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700"
      rel="stylesheet"
    />

    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>

    <!-- Bootstrap CSS File -->
    <link href="../css/bootstrap.min.css" rel="stylesheet" />

    <!-- FontAwesome -->
    <link
      rel="stylesheet"
      href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
      integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay"
      crossorigin="anonymous"
    />

    <!-- Main Stylesheet File -->
    <link href="../css/style.css" rel="stylesheet" />

    <!-- Bootstrap core CSS -->
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
      crossorigin="anonymous"
    />

    <!-- Alert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  </head>

  <body>
<!-- TODO Social Media Verlinkung -->
<!-- Footer -->
    <footer id="page-footer" class="py-4">
      <!-- Footer Elements -->
      <div class="container pt-3 pb-1">
        <!-- Grid row-->
        <div class="row" id="footermedia">
          <!-- Grid column -->

          <div class="col-md-12 pt-2 text-center">
            <div class="mb-2 align-items-center">
              <!--Facebook-->
              <a href="https://www.twitch.tv/playportal_network">
                <i class="fab fa-facebook fa-lg white-text mx-4 fa-2x"> </i>
              </a>
              <!--Instagram-->
              <a href="https://www.instagram.com/PlayportalNetwork/">
                <i class="fab fa-instagram fa-lg white-text mx-4 fa-2x"> </i>
              </a>
              <!--Twitter-->
              <a href="https://twitter.com/playportalN">
                <i class="fab fa-twitter fa-lg white-text mx-4 fa-2x"> </i>
              </a>
            </div>
          </div>
          <!-- Grid column -->
        </div>
        <!-- Grid row-->

        <!-- Grid row-->
        <div class="row" id="footermedia">
          <!-- Grid column -->
          <div class="col-md-12 py-1 text-center">
            <a href="impressum.php">Impressum</a>
          </div>
          <div class="col-md-12 py-1 text-center">
            <a href="datenschutz.php">Datenschutz</a>
            <!-- Grid column -->
          </div>
          <!-- Grid row-->
        </div>
        <!-- Footer Elements -->

        <!-- Copyright -->
        <div class="footer-copyright text-center py-2">
          <p>
            © Copyright <b><a href="">Melvin Lee Lewis</a></b>
          </p>
        </div>
        <!-- Copyright -->
      </div>
    </footer>
    <!-- #footer -->

    <script
      src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
      integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
      integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
      integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
      crossorigin="anonymous"
    ></script>
  </body>
</html>

<!DOCTYPE html>
<html lang="de">
  <head>
    <title>Melvin Lee Lewis</title>
  </head>

  <body>
    <div class="jumbotron" style="margin-bottom: 0px;">
      <h1 class="brand">Melvin Lee Lewis</h1>
      <p class="brand">Schauspieler - Model</p>
    </div>

    <nav class="navbar navbar-expand-md navbar-dark bg-dark">
      <button
        class="navbar-toggler"
        data-toggle="collapse"
        data-target="#collapse_target"
      >
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="collapse_target">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="../index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="galerie.php">Galerie</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="referenzen.php">Referenzen</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="resume.php">Résumé</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="events.php">Events</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="kontakt.php">Kontakt</a>
          </li>
        </ul>
      </div>
    </nav>

    <div class="red_color"></div>

  </body>
</html>

<?php
  require 'default.php';
?>

<!DOCTYPE html>
<html lang="de">
  <head>
    <title>Melvin Lee Lewis</title>
  </head>

  <body>

<?php
  require 'navbar.php';
?>


    <main role="main">
      <section id="contact" class="section-bg">
        <div class="container-fluid">
          <div class="section-header">
            <h3 id="kontakt_h3">Kontakt</h3>
          </div>

          <div class="row wow fadeInUp">
            <div class="col-lg-6">
              <div class="form">
                <form
                  action="contactform.php"
                  method="post"
                  accept-charset="UTF-8"
                  role="form"
                  class="contactForm"
                >
                  <div class="form-row">
                    <div class="form-group col-lg-6">
                      <input
                        type="text"
                        name="name"
                        class="form-control"
                        id="name"
                        placeholder="Ihr Name"
                      />
                    </div>
                    <div class="form-group col-lg-6">
                      <input
                        type="email"
                        class="form-control"
                        name="email"
                        id="email"
                        placeholder="Ihre Email-Adresse"
                        data-rule="email"
                        data-msg="Please enter a valid email"
                      />
                    </div>
                  </div>
                  <div class="form-group">
                    <input
                      type="text"
                      class="form-control"
                      name="subject"
                      id="subject"
                      placeholder="Betreff"
                    />
                  </div>
                  <div class="form-group">
                    <textarea
                      class="form-control"
                      name="message"
                      rows="5"
                      placeholder="Nachricht"
                    ></textarea>
                  </div>
                  <div class="text-center">
                    <button type="submit" title="Send Message">
                      Nachricht senden
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- #contact -->
    </main>
    <!-- /.container -->

    <div class="row">
      <div class="col-sm-6 center">
            <img src="../img/contact.jpg" class="img-thumbnail">
      </div>
    </div>


  </body>
</html>

<?php
  require 'default.php';
?>

  </body>
</html>

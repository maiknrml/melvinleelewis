<!DOCTYPE html>
<html lang="de">

<head>
    <title>Melvin Lee Lewis</title>
    <!-- Bootstrap CSS File -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <!-- Main Stylesheet File -->
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/video-header.css" rel="stylesheet" />
</head>

<body>

<body>
    <!-- <div class="jumbotron" style="margin-bottom: 0px;">
      <h1 class="brand">Melvin Lee Lewis</h1>
      <p class="brand">Schauspieler - Model</p>
    </div> -->

<header>
  <div class="overlay"></div>
  <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
    <source src="https://storage.googleapis.com/coverr-main/mp4/Mt_Baker.mp4" type="video/mp4">
  </video>
  <div class="container h-100">
    <div class="d-flex h-100 text-center align-items-center">
      <div class="w-100 text-white">
        <h1 class="display-3">Melvin Lee Lewis</h1>
        <p class="lead mb-0">Schaupieler - Model</p>
      </div>
    </div>
  </div>
</header>


<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <button class="navbar-toggler" data-toggle="collapse" data-target="#collapse_target">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapse_target">
    <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="html/galerie.php">Galerie</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="html/referenzen.php">Referenzen</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="html/resume.php">Résumé</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="html/events.php">Events</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="html/kontakt.php">Kontakt</a>
          </li>
        </ul>
    </div>
</nav>
<div class="graueBox"></div>
<div class="graueBox"></div>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-6 center home_p">
          <h1 class="home_h1"> Über mich </h1>
          <br>
            <p class="output"> 
              
            </p>
        </div>
    </div>

    <div class="row">
      <div class="col-sm-6 center">
            <!-- img -->
      </div>
      <div class="col-sm-6 center">
            <!-- img -->
      </div>
    </div>
</div>

<div class="graueBox"></div>
<div class="graueBox"></div>
  

<script>

  window.onload = function output() {
    var text = "Mein Name ist Melvin Lee Lewis und bin 20 Jahre alt. Im Jahr 2017 habe ich mein Germanistik- und Geologie-Studium auf Lehramt-Basis begonnen und bin deswegen nach Trier gezogen. Dort habe ich einige neue Menschen kennengelernt. Durch viele Zufälle bin ich letztendlich auf eine Rolle als Schauspieler gestoßen. Mein erster Drehtag hat mir unglaublich Spaß gemacht, dass ich im Laufe der Zeit eine große Leidenschaft für das Schauspiel entwickelt habe. Mittlerweile konnte ich schon bei mehreren namenhaften Projekten mitarbeiten, seien es die E-Learning Serie auf BBC, Werbespots von CoinMaster oder BremenNext und sogar bei einem Kinofilm konnte ich mitspielen.";
    var output = document.querySelector('.output');
    output.innerHTML = text;
  }

</script>


</body>
</html>

<?php
require 'html/default.php'
?>